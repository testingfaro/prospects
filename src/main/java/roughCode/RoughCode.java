package roughCode;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;

public class RoughCode {
	@FindBy(xpath="//input[@type='email']")
	 public static WebElement txtEmail;
	@FindBy (xpath="//input[@type='password']")
	 public static WebElement txtPassword;
	@FindBy (xpath="//button[@type='submit']")
	 public static WebElement btnSubmit;
	public static void main(String[] args) {
		String userDir = System.getProperty("user.dir");
		String browserPath = userDir + "\\src\\test\\resources\\browserDrivers\\chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", browserPath);
		WebDriver driver = new ChromeDriver();
		
		driver.get("https://accounts-pgdev.outplayhq.com/login");
		txtEmail.sendKeys("test");
		txtPassword.sendKeys("test");
		btnSubmit.click();
		
		
		/*driver.findElement(By.xpath("//input[@type='email']")).sendKeys("outplaytest@martinz.co.in");
		driver.findElement(By.xpath("//input[@type='password']")).sendKeys("Suresh123$");
		driver.findElement(By.xpath("//button[@type='submit']")).click();*/
		
		

	}

}
