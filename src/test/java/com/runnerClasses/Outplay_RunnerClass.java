package com.runnerClasses;

import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;




@CucumberOptions ( 
				features = {"src/test/resources/featureFiles"},
				tags = "@create", 
				glue = {"com.stepDefinitions"},
				plugin = {"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:"},
				dryRun = false, 
				monochrome = true
				)

public class Outplay_RunnerClass extends AbstractTestNGCucumberTests {
	
	

		
}
