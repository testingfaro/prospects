package com.stepDefinitions;

import java.io.IOException;
import java.util.Map;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.pageClasses.ProspectsPageClass;

import io.cucumber.java.Before;
import io.cucumber.java.en.*;

public class Prospects_Stepdefinition {
	CommonMethods cmnmthds = new CommonMethods();
	//WebDriver driver = cmnmthds.getDriver();
	WebDriver driver = cmnmthds.getBrowser();
	ProspectsPageClass prospObj = new ProspectsPageClass(driver);


	@Given("user is on outplay login page in environment")
	public void user_is_on_outplay_login_page_in_environment(io.cucumber.datatable.DataTable dataTable) throws IOException {
		Map <String,String> data = dataTable.asMap(String.class, String.class);
		prospObj.navigateURL(data.get("environment"));
	}

	@When("user log in with valid credentials")
	public void user_log_in_with_valid_credentials(io.cucumber.datatable.DataTable dataTable) throws IOException, InterruptedException {
		Map <String,String> data = dataTable.asMap(String.class, String.class);
		prospObj.vldcredentials(data.get("username"), data.get("password"));
		prospObj.elmtgetText(prospObj.msgDashboard, 5000, "Dashboard");
	}

	@Then("Prospects is shown on the prospects page")
	public void prospects_is_shown_on_the_prospects_page() {
		prospObj.elmtClick(prospObj.iconProspects, 2000);
		prospObj.elmtgetText(prospObj.msgProspects, 3000, "Prospects");
	}

	@Given("user is on Add Prospect modal window")
	public void user_is_on_Add_Prospect_modal_window() {
		prospObj.elmtClick(prospObj.btnAddProsp, 2000);
		prospObj.elmtgetText(prospObj.msgAddProsp, 1000, "Add Prospect");
	}

	@When("user fill all the details {string} {string} {string}")
	public void user_fill_all_the_details(String firstName, String lastName, String company) throws InterruptedException {
		prospObj.addProspect(firstName, lastName, company);
	}

	@When("create prospect {string} {string} {string}")
	public void create_prospect(String firstName, String lastName, String company) {
		prospObj.elmtClick(prospObj.btnCreate, 0);
		prospObj.elmtgetText(prospObj.alrtDialog, 2000, "Prospect Saved successfully.");
		WebElement lnkCrtdProsp = driver.findElement(By.xpath("//a[normalize-space()='"+firstName+" "+lastName+"']"));
		prospObj.isElmntDisplayed(lnkCrtdProsp);
	}

	@Then("prospect is created")
	public void prospect_is_created() {
		System.out.println("Prospect is created");
	}

	@When("cancel prospect {string} {string} {string}")
	public void cancel_prospect (String firstName, String lastName, String company) throws InterruptedException {
		prospObj.elmtClick(prospObj.btnCancel, 0);
		prospObj.isElmntDisplayed(prospObj.lnkcldProsp);

	}

	@Then("prospect is not created")
	public void prospect_is_not_created() {
		System.out.println("Prospect is not created");
	}
	@When("user create prospect without any values")
	public void user_create_prospect_without_any_values() {
		prospObj.elmtClick(prospObj.btnCreate, 0);
	}

	@Then("error message is shown")
	public void error_message_is_shown(String docString) {

		prospObj.elmtgetText(prospObj.alrtDialog, 2000, docString);
	}

	@When("user creates a prospect with only email {string} {string}")
	public void user_creates_a_prospect_with_only_email(String firstName, String lastName) throws InterruptedException {
		prospObj.addProspwithemail(firstName, lastName);
		prospObj.prospectsavedmsg();
		WebElement lnkCrtdProsp = driver.findElement(By.xpath("//a[normalize-space()='"+firstName+" "+lastName+"']"));
		prospObj.isElmntDisplayed(lnkCrtdProsp);

	}

	@Given("user is on prospects screen")
	public void user_is_on_prospects_screen() {
		System.out.println("User is on prospect screen");
	}



	@When("user creates a prospect with only phone number {string} {string}")
	public void user_creates_a_prospect_with_only_phone_number(String firstName, String lastName) throws InterruptedException {
		prospObj.addProspwithph(firstName, lastName);
		prospObj.prospectsavedmsg();
		WebElement lnkCrtdProsp = driver.findElement(By.xpath("//a[normalize-space()='"+firstName+" "+lastName+"']"));
		prospObj.isElmntDisplayed(lnkCrtdProsp);
	}

	@When("user creates a prospect with only linkedin {string} {string}")
	public void user_creates_a_prospect_with_only_linkedin(String firstName, String lastName) throws InterruptedException {
		prospObj.addProspwithlinkedin(firstName, lastName);
		prospObj.prospectsavedmsg();
		WebElement lnkCrtdProsp = driver.findElement(By.xpath("//a[normalize-space()='"+firstName+" "+lastName+"']"));
		prospObj.isElmntDisplayed(lnkCrtdProsp);
	}

	@When("user creates prospect with existing email id {string} {string}")
	public void user_creates_prospect_with_existing_email_id(String firstName, String lastName) throws InterruptedException {
		prospObj.addProspwithemail(firstName, lastName);

	}

	@When("user creates prospect with existing linkedin id {string} {string}")
	public void user_creates_prospect_with_existing_linkedin_id(String firstName, String lastName) throws InterruptedException {
		prospObj.addProspwithlinkedin(firstName, lastName);


	}

	@When("user creates a prospect with email & without company, account {string} {string}")
	public void user_creates_a_prospect_with_email_without_company_account(String firstName, String lastName) throws InterruptedException {
		prospObj.prospnamewithEmail(firstName, lastName);
	}

	@When("user creates a prospect with email, company but not account {string} {string} {string}")
	public void user_creates_a_prospect_with_email_company_but_not_account(String firstName, String lastName, String company) throws InterruptedException {
		prospObj.elmtClick(prospObj.crtdProsp, 1000);
		Thread.sleep(3000);
		Actions actions = new Actions(driver);
		actions.moveToElement(prospObj.lnkbtnindshowmore).click().build().perform();
		Thread.sleep(3000);
		actions.moveToElement(prospObj.inptindCompany).click().build().perform();
		prospObj.prospnamewithCompany();
			
	}
	
	@When("user creates a prospect with email, company & account {string} {string} {string} {string}")
	public void user_creates_a_prospect_with_email_company_account(String string, String string2, String string3, String string4) throws InterruptedException {
		prospObj.elmtClick(prospObj.crtdProsp, 1000);
		Actions actions = new Actions(driver);
		actions.moveToElement(prospObj.btnindAccount).click().build().perform();
		prospObj.prospnamewithAccount();
				
	}

	@Then("prospect is created with prospect names accordingly")
	public void prospect_is_created_with_prospect_names_accordingly() {
	   System.out.println("Account names are displaying as expected");
	}

	@When("user select all prospects in a page")
	public void user_select_all_prospects_in_a_page() throws InterruptedException {
		prospObj.selectProspects();
		}
	
	@Then("all prospects in the current page is selected")
	public void all_prospects_in_the_current_page_is_selected() throws InterruptedException {
		int slctallsze1 = prospObj.btnSlctAlllst.size();
		System.out.println("size of element is" + slctallsze1);
		Assert.assertEquals(slctallsze1, 1);
	}
	
	@When("clear the selection")
	public void clear_the_selection() {
		prospObj.clearSelection();
	}
	
	@Then("all prospects in the current page is deselected")
	public void all_prospects_in_the_current_page_is_deselected() {
		prospObj.isElmntDisplayed(prospObj.btnclrSelct);
	}
	
	@When("user select all prospects in the prospect list")
	public void user_select_all_prospects_in_the_prospect_list() throws InterruptedException {
		prospObj.selectallProspects();
	}
	
	@Then("all prospects in the prospect list are selected")
	public void all_prospects_in_the_prospect_list_are_selected() throws InterruptedException {
		Thread.sleep(2000);
		int slctallsze = prospObj.btnSlctAlllst.size();
		Assert.assertEquals(slctallsze, 0);
	}
	
	@Then("all prospects in the prospect list are deselected")
	public void all_prospects_in_the_prospect_list_are_deselected() {
		prospObj.isElmntDisplayed(prospObj.btnclrSelct);
	}
	
	@When("user search prospect using prospect name {string} {string}")
	public void user_search_prospect_using_prospect_name(String firstName, String lastName) throws InterruptedException {
		prospObj.srchProspwithName(firstName, lastName);
	}
	
	@Then("Prospect with relevant name is shown")
	public void prospect_with_relevant_name_is_shown() {
		prospObj.elmtgetText(prospObj.msgTotal, 3000, "1");
	}
	
	@When("user search prospect using emailid {string} {string}")
	public void user_search_prospect_using_emailid(String firstName, String lastName) throws InterruptedException {
		prospObj.srchProspwithEmail(firstName, lastName);
	}
	
	@Then("Prospect with relevant emailid is shown")
	public void prospect_with_relevant_emailid_is_shown() {
		prospObj.elmtgetText(prospObj.msgTotal, 3000, "1");
	}
	
	@When("user search prospect using values other than prospect name & email")
	public void user_search_prospect_using_values_other_than_prospect_name_email() throws InterruptedException {
		prospObj.srchProspwithinval();
	}
	
	@Then("Prospect is not shown")
	public void prospect_is_not_shown() {
		prospObj.elmtgetText(prospObj.msgnoProsp, 1000, "Oops! No prospects found.");
	}
	
	@When("user selects any prospect")
	public void user_selects_any_prospect() {
		prospObj.elmtClick(prospObj.chkbxProspSel, 1000);
	}
	
	@Then("search prospect field is disappeared")
	public void search_prospect_field_is_disappeared() {
		prospObj.isElmntDisplayed(prospObj.blckProspinline);
	}
}
