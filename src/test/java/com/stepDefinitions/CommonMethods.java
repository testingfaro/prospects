package com.stepDefinitions;

import static org.testng.Assert.assertEquals;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Properties;



import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.pageClasses.LoginPageClass;

import io.cucumber.java.*;

public class CommonMethods {

	public static WebDriver driver = null;
	public WebDriverWait wait;
	public String userDir = System.getProperty("user.dir");
	public static Properties prop = new Properties();
	private static CommonMethods instCmnmethds;
	/*private  void initDriv() {
		try {
			getPropFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		String driverPath = userDir + "\\src\\test\\resources\\browserDrivers";
		String browserName = prop.getProperty("browser");

		switch (browserName) {
		case "Firefox":
			System.setProperty("webdriver.gecko.driver", driverPath + "\\geckodriver.exe");
			driver = new FirefoxDriver();
			driver.manage().window().maximize();
			driver.manage().deleteAllCookies();
			break;
		case "IE":
			System.setProperty("webdriver.ie.driver",driverPath + "\\IEDriverServer.exe");
			driver = new InternetExplorerDriver();
			driver.manage().window().maximize();
			driver.manage().deleteAllCookies();
			break;
		case "Chrome":
			System.setProperty("webdriver.chrome.driver", driverPath + "\\chromedriver.exe");
			driver = new ChromeDriver();
			driver.manage().window().maximize();
			driver.manage().deleteAllCookies();
			break;
		case "Edge":
			System.setProperty("webdriver.edge.driver", driverPath + "\\msedgedriver.exe");
			driver = new EdgeDriver();
			driver.manage().window().maximize();
			driver.manage().deleteAllCookies();
			break;
		default:
			System.out.println("Drivers not initiated");

		}

	}

	public static CommonMethods getInst () {
		if(instCmnmethds==null) {
			instCmnmethds = new CommonMethods ();
		}
		return instCmnmethds;
	}
	public WebDriver getDriver()
	{
		return driver;
	}*/


	public Properties getPropFile() throws IOException {

		String filePath = userDir + "\\src\\test\\resources\\propertiesFiles\\project.properties";
		FileInputStream fis = new FileInputStream (filePath);
		prop.load(fis);
		return prop;
	}




	public WebDriver getBrowser() {
		try {
			getPropFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		String driverPath = userDir + "\\src\\test\\resources\\browserDrivers";
		String browserName = prop.getProperty("browser");

		switch (browserName) {
		case "Firefox":
			System.setProperty("webdriver.gecko.driver", driverPath + "\\geckodriver.exe");
			driver = new FirefoxDriver();
			driver.manage().window().maximize();
			driver.manage().deleteAllCookies();
			break;
		case "IE":
			System.setProperty("webdriver.ie.driver",driverPath + "\\IEDriverServer.exe");
			driver = new InternetExplorerDriver();
			driver.manage().window().maximize();
			driver.manage().deleteAllCookies();
			break;
		case "Chrome":
			System.setProperty("webdriver.chrome.driver", driverPath + "\\chromedriver.exe");
			driver = new ChromeDriver();
			driver.manage().window().maximize();
			driver.manage().deleteAllCookies();
			break;
		case "Edge":
			System.setProperty("webdriver.edge.driver", driverPath + "\\msedgedriver.exe");
			driver = new EdgeDriver();
			driver.manage().window().maximize();
			driver.manage().deleteAllCookies();
			break;
		default:
			System.out.println("Drivers not initiated");

		}

		return driver;
	}


	public void inputData (WebElement locator, String txt) throws InterruptedException {
		locator.click();	
		locator.clear();
		locator.sendKeys(txt);
	}



	public void navigateURL (String url) throws IOException {
		String env = prop.getProperty(url);
		driver.get(env);
	}

	public void explicitWait(WebElement locator, int timeUnit ) {
		wait = new WebDriverWait  (driver, timeUnit);
	}


	public void elmtClick (WebElement locator, int clkwaitTime) {
		explicitWait(locator, clkwaitTime);
		wait.until(ExpectedConditions.elementToBeClickable(locator));
		locator.click();
	}

	public void elmtgetText (WebElement locator, int txtwaitTime, String verifytxt) {
		explicitWait(locator, txtwaitTime);
		//wait.until(ExpectedConditions.textToBePresentInElement(locator, verifytxt));
		wait.until(ExpectedConditions.visibilityOf(locator));
		String msg = locator.getText();
		System.out.println(msg);
		Assert.assertEquals(msg, verifytxt);
	}

	public void isElmntDisplayed (WebElement locator) {

		try {
			if(locator.isDisplayed()) {
				System.out.println(locator + "is displayed");
			}

		} catch (Exception e) {
			System.out.println(locator + "is not displayed");
			Assert.assertTrue(false);
			
			
		}
	}

	public void isElmntNotDisplayed (List<WebElement> locator) {
	
		int size = locator.size();
		Assert.assertEquals(size, 0);
	}
	




	//@After(order=0)
	public void closeAllDriver() {

		driver.close();
		driver.quit();
	}

	@After(order=1)
	public void screenshot(Scenario scenario) {
		if (scenario.isFailed()) {
			final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
			scenario.attach(screenshot, "image/png", "Failed_screenshot");
		}
	}














}
