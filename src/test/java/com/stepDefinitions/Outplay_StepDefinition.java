package com.stepDefinitions;

import java.io.IOException;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;

import com.pageClasses.LoginPageClass;

import io.cucumber.java.After;
import io.cucumber.java.Scenario;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class Outplay_StepDefinition  {
	CommonMethods cmnmthds = new CommonMethods ();
	WebDriver driver = cmnmthds.getBrowser();
	LoginPageClass loginObj = new LoginPageClass(driver);

	@Given("user is on outplay login page in {string} environment")
	public void user_is_on_outplay_login_page_in_environment(String environment) throws IOException {
		
		loginObj.navigateURL(environment);

	}

	@When("user enters valid {string},{string} and clicks login")
	public void user_enters_valid_and_clicks_login(String username, String password) throws IOException, InterruptedException {

		loginObj.vldcredentials(username, password);

	}

	@Then("Dashboard is shown on the welcome page")
	public void Dashboard_is_shown_on_the_welcome_page() {

		loginObj.elmtgetText(loginObj.msgDashboard, 5000, "Dashboard");

	}

	@When("user enters invalid {string},{string} and clicks login")
	public void user_enters_invalid_and_clicks_login(String username, String password) throws IOException, InterruptedException {

		loginObj.invcredentials(username, password);

	}

	@Then("Error message is shown")
	public void error_message_is_shown() {

		loginObj.elmtgetText(loginObj.alrtinvlogin, 5000, "Invaild Email or Password");

	}











}
