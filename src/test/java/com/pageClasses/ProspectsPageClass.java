package com.pageClasses;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.*;

import com.stepDefinitions.CommonMethods;

import junit.framework.Assert;

public class ProspectsPageClass extends LoginPageClass {


	CommonMethods cmnmethods = new CommonMethods();
	public WebDriver driver;

	@FindBy(how=How.XPATH, using = "//i[@class='far fa-user']")
	public WebElement iconProspects;
	@FindBy(how=How.XPATH, using = "//span[normalize-space()='Prospects']")
	public WebElement msgProspects;
	@FindBy(how=How.XPATH, using = "//button[normalize-space()='Add Prospect']")
	public WebElement btnAddProsp;
	@FindBy(how=How.XPATH, using = "//h5[normalize-space()='Add Prospect']")
	public WebElement msgAddProsp;
	@FindBy(how=How.XPATH, using = "//input[@formcontrolname='firstname']")
	public WebElement inptFirstName;
	@FindBy(how=How.XPATH, using = "//input[@formcontrolname='lastname']")
	public WebElement inptLastName;
	@FindBy(how=How.XPATH, using = "//input[@formcontrolname='emailid']")
	public WebElement inptEmail;
	@FindBy(how=How.XPATH, using = "(//div[@class='selected-flag']//div)[3]")
	public WebElement ddwnFlag;
	@FindBy(how=How.XPATH, using = "//span[@class='select2-search select2-search--dropdown']//input[@role='textbox']")
	public WebElement inptFlag;
	@FindBy(how=How.XPATH, using = "//input[@type='tel']")
	public WebElement inptPh;
	@FindBy(how=How.XPATH, using = "//span[text()=' Show more options ']")
	public WebElement lnkbtnShowmore;
	@FindBy(how=How.XPATH, using = "(//div[@id='extraInputs']//input)[1]")
	public WebElement inptLinkedin;
	@FindBy(how=How.XPATH , using = "(//div[@id='extraInputs']//input)[2]")
	public WebElement inptFacebook;
	@FindBy(how=How.XPATH , using = "(//div[@id='extraInputs']//input)[3]")
	public WebElement inptTwitter;
	@FindBy(how=How.XPATH, using = "(//div[@id='extraInputs']//input)[4]")
	public WebElement inptCompany;
	@FindBy(how=How.XPATH, using = "(//div[@id='extraInputs']//input)[5]")
	public WebElement inptCity;
	@FindBy(how=How.XPATH, using = "(//div[@id='extraInputs']//input)[6]")
	public WebElement inptState;
	@FindBy(how=How.XPATH, using = "(//div[@id='extraInputs']//input)[7]")
	public WebElement inptCountry;
	@FindBy(how=How.XPATH, using = "//button[normalize-space()='Create']")
	public WebElement btnCreate;
	@FindBy(how=How.XPATH, using = "(//a[text()='Cancel'])[2]")
	public WebElement btnCancel;
	@FindBy (how=How.XPATH, using="//div[@role='alertdialog']")
	public WebElement alrtDialog;
	@FindBy (how=How.XPATH, using="//a[normalize-space()='Great Expectations']")
	public WebElement lnkcldProsp;
	@FindBy (how=How.XPATH, using="//input[@id='prospectTextSearch']")
	public WebElement inputProspectSearch;
	@FindBy (how=How.XPATH, using="//span[@class='search-field-container']//i[@class='far fa-search']")
	public WebElement srchProspMagnifier;
	@FindBy (how=How.XPATH, using="//div[@class='filter-content-data']//a")
	public WebElement crtdProsp;
	@FindBy (how=How.XPATH, using="(//div[@class='filter-content-data']//span)[3]")
	public WebElement lnkspan;
	@FindBy (how=How.XPATH, using="//a[@class='nav-link active']//div[@class='stats'][normalize-space()='1']")
	public WebElement msgTotal;
	@FindBy (how=How.XPATH, using="//span[@class='blue-link font-14 d-inline-block mb-15px']")
	public WebElement lnkbtnindshowmore;
	@FindBy (how=How.XPATH, using="//input[@placeholder='No company']")
	public WebElement inptindCompany;
	@FindBy (how=How.XPATH, using="//a[@class='fas fa-times prospect-sidebar-header-buttons close-button']")
	public WebElement btnClose;
	@FindBy (how=How.XPATH, using="//span[@class='btn-with-checkbox mr-10px r-refresh-btn']")
	public WebElement btnRefresh;
	@FindBy (how=How.XPATH, using="//input[@placeholder='No account']")
	public WebElement btnindAccount;
	@FindBy (how=How.XPATH, using="//ng-select2[@id='accountSelect2Dir']//span[@role='presentation']")
	public WebElement ddnindAccount;
	@FindBy (how=How.XPATH, using="(//input[@class='select2-search__field'])[3]")
	public WebElement inptindAccount;
	@FindBy (how=How.XPATH, using="//span[@id='accountField']")
	public WebElement btnindAcctsve;
	@FindBy (how=How.XPATH, using="//span[normalize-space()='Clear All']")
	public WebElement btnClrAll;
	@FindBy (how=How.XPATH, using="//label[@class='CheckboxContainer mb-0']//span[@class='CheckedMark']")
	public WebElement chkbxSelect;
	@FindBy (how=How.XPATH, using="//a[normalize-space()='All']")
	public WebElement ddnlstAll;
	@FindBy (how=How.XPATH, using="//a[normalize-space()='None']")
	public WebElement ddnlstNone;
	@FindAll ({@FindBy (how=How.XPATH, using="//span[contains(text(),'Select all')]")
	})
	public List<WebElement> btnSlctAlllst;
	@FindBy (how=How.XPATH, using="//span[normalize-space()='Clear selection']")
	public WebElement btnclrSelct;
	@FindBy (how=How.XPATH, using= "//span[@class='nothing-found d-block']")
	public WebElement msgnoProsp;
	@FindBy (how=How.XPATH, using= "(//tr//div[@class='Checkbox-selector'])[1]")
	public WebElement chkbxProspSel;
	@FindAll ({@FindBy (how=How.XPATH, using="//input[@id='prospectTextSearch']")
	})
	public List<WebElement> inputProspectSearchlst;
	@FindBy (how=How.XPATH, using= "//div[@class='d-inline-block']")
	public WebElement blckProspinline;
	
	
	
	
















	public ProspectsPageClass (WebDriver driver) {
		super(driver);
	}

	public void addProspect(String firstName, String lastName, String company) throws InterruptedException {
		cmnmethods.inputData(inptFirstName, firstName);
		cmnmethods.inputData(inptLastName, lastName);
		cmnmethods.inputData(inptEmail, firstName+"2000@martinz.co.in");
		cmnmethods.elmtClick(ddwnFlag, 2000);
		cmnmethods.inputData(inptFlag, "India");
		inptFlag.sendKeys(Keys.ARROW_DOWN);
		inptFlag.sendKeys(Keys.ENTER);
		cmnmethods.inputData(inptPh, "9942016321");
		cmnmethods.elmtClick(lnkbtnShowmore, 1000);
		cmnmethods.elmtClick(inptPh,1000);
		inptPh.sendKeys(Keys.TAB);
		inptPh.sendKeys(Keys.TAB);
		inptPh.sendKeys(Keys.TAB);
		cmnmethods.inputData(inptLinkedin, "https://linkedin.com");
		cmnmethods.inputData(inptFacebook, "https://facebook.com");
		cmnmethods.inputData(inptTwitter, "https://twitter.com");
		cmnmethods.inputData(inptCompany, company);
		cmnmethods.inputData(inptCity, "Chennai");
		cmnmethods.inputData(inptState, "Tamilnadu");
		cmnmethods.inputData(inptCountry, "India");

	}
	public void prospectsavedmsg () {
		cmnmethods.elmtgetText(alrtDialog, 2000, "Prospect Saved successfully.");
	}
	public void addProspwithemail(String firstName, String lastName) throws InterruptedException {
		cmnmethods.inputData(inptFirstName, firstName);
		cmnmethods.inputData(inptLastName, lastName);
		String email = firstName+"2000@martinz.co.in" ;
		cmnmethods.inputData(inptEmail, email);
		cmnmethods.elmtClick(btnCreate, 0);
		//cmnmethods.elmtgetText(alrtDialog, 2000, "Prospect Saved successfully.");
	}
	public void addProspwithph(String firstName, String lastName) throws InterruptedException {
		cmnmethods.inputData(inptFirstName, firstName);
		cmnmethods.inputData(inptLastName, lastName);
		cmnmethods.elmtClick(ddwnFlag, 2000);
		cmnmethods.inputData(inptFlag, "India");
		inptFlag.sendKeys(Keys.ARROW_DOWN);
		inptFlag.sendKeys(Keys.ENTER);
		cmnmethods.inputData(inptPh, "9942016321");
		cmnmethods.elmtClick(btnCreate, 0);
		//cmnmethods.elmtgetText(alrtDialog, 2000, "Prospect Saved successfully.");
	}
	public void addProspwithlinkedin(String firstName, String lastName) throws InterruptedException {
		cmnmethods.inputData(inptFirstName, firstName);
		cmnmethods.inputData(inptLastName, lastName);
		cmnmethods.elmtClick(lnkbtnShowmore, 1000);
		cmnmethods.elmtClick(inptPh,1000);
		inptPh.sendKeys(Keys.TAB);
		inptPh.sendKeys(Keys.TAB);
		inptPh.sendKeys(Keys.TAB);
		cmnmethods.inputData(inptLinkedin, "https://linkedin"+firstName+".com");
		cmnmethods.elmtClick(btnCreate, 0);
		//cmnmethods.elmtgetText(alrtDialog, 2000, "Prospect Saved successfully.");
	}

	public void prospnamewithEmail (String firstName, String lastName) throws InterruptedException {
		addProspwithemail(firstName, lastName);
		prospectsavedmsg();
		cmnmethods.inputData(inputProspectSearch, ""+firstName+"2000@martinz.co.in");
		inputProspectSearch.sendKeys(Keys.ENTER);
		cmnmethods.elmtgetText(msgTotal, 3000, "1");	
		cmnmethods.elmtgetText(lnkspan, 3000, " @ martinz.co.in");
	}

	public void prospnamewithCompany () throws InterruptedException {
		cmnmethods.inputData(inptindCompany, "Tortoise");
		inptindCompany.sendKeys(Keys.ENTER);
		cmnmethods.elmtgetText(alrtDialog, 2000, "Prospect updated successfully.");
		cmnmethods.elmtClick(btnClose, 1000);
		Thread.sleep(3000);
		cmnmethods.elmtClick(btnRefresh, 2000);
		Thread.sleep(3000);
		cmnmethods.elmtgetText(lnkspan, 3000, " @ Tortoise");

	}
	public void prospnamewithAccount () throws InterruptedException {
		cmnmethods.elmtClick(ddnindAccount, 1000);
		cmnmethods.inputData(inptindAccount, "Existing");
		inptindAccount.sendKeys(Keys.ENTER);
		cmnmethods.elmtClick(btnindAcctsve, 1000);
		cmnmethods.elmtgetText(alrtDialog, 2000, "Prospect updated successfully.");
		cmnmethods.elmtClick(btnClose, 1000);
		Thread.sleep(3000);
		cmnmethods.elmtClick(btnRefresh, 2000);
		Thread.sleep(3000);
		cmnmethods.elmtgetText(lnkspan, 3000, " @ Existing Account");

	}

	public void selectProspects () throws InterruptedException {
		cmnmethods.elmtClick(btnClrAll, 1000);
		cmnmethods.elmtClick(chkbxSelect, 1000);
		cmnmethods.elmtClick(ddnlstAll, 1000);
	}
	public void selectallProspects () throws InterruptedException {
		cmnmethods.elmtClick(btnClrAll, 1000);
		cmnmethods.elmtClick(chkbxSelect, 1000);
		cmnmethods.elmtClick(ddnlstAll, 1000);
		int slctallsze = btnSlctAlllst.size();
		Assert.assertEquals(slctallsze, 1);
		for (WebElement e : btnSlctAlllst ) {
			e.click();
		}
	}
	public void clearSelection () {
		cmnmethods.elmtClick(chkbxSelect, 1000);
		cmnmethods.elmtClick(ddnlstNone, 1000);
	}

	public void srchProspwithName(String firstName, String lastName) throws InterruptedException {

		addProspwithemail(firstName, lastName);
		cmnmethods.elmtgetText(alrtDialog, 2000, "Prospect Saved successfully.");
		cmnmethods.inputData(inputProspectSearch,firstName+" "+ lastName );
		inputProspectSearch.sendKeys(Keys.ENTER);	
	}	

	public void srchProspwithEmail(String firstName, String lastName) throws InterruptedException {

		cmnmethods.inputData(inputProspectSearch,firstName+"2000@martinz.co.in" );
		inputProspectSearch.sendKeys(Keys.ENTER);
	}

	public void srchProspwithinval() throws InterruptedException {

		Thread.sleep(2000);
		cmnmethods.inputData(inputProspectSearch,"{}" );
		inputProspectSearch.sendKeys(Keys.ENTER);
	}











}
