
package com.pageClasses;

import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.stepDefinitions.CommonMethods;

public class LoginPageClass extends CommonMethods  {
	public WebDriver driver;
	CommonMethods cmnmethods = new CommonMethods();
	
	
	@FindBy (how=How.XPATH, using ="//h3[text()='Log In']")
	 public WebElement lblLogin;
	@FindBy(how=How.XPATH, using ="//input[@type='email']")
	 public WebElement inptEmail;
	@FindBy (how=How.XPATH, using ="//input[@type='password']")
	 public WebElement inptPassword;
	@FindBy (how=How.XPATH, using ="//button[@type='submit']")
	 public WebElement btnSubmit;
	@FindBy (how=How.XPATH, using ="//span[normalize-space()='Dashboard']")
	 public WebElement msgDashboard;
	@FindBy (how=How.XPATH, using="//div[@aria-label='Invalid Email or Password']")
	 public WebElement alrtinvlogin;
	@FindAll ({@FindBy (how=How.XPATH, using="//div[@aria-label='Invalid Email or Password']")
	})
	public List<WebElement> alrtinvloginlst;
	
	public LoginPageClass (WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}
		
	public void vldcredentials(String username, String password) throws IOException, InterruptedException {
		Assert.assertTrue(lblLogin.isDisplayed());
		//cmnmethods.getPropFile();
		String emailID = prop.getProperty("uName");
		String  pwdtxt = prop.getProperty("pWord");
		cmnmethods.inputData(inptEmail, emailID);
		cmnmethods.inputData(inptPassword, pwdtxt);	
		cmnmethods.elmtClick(btnSubmit, 1000);
		cmnmethods.isElmntNotDisplayed(alrtinvloginlst);
		
	}
	
	
	
	public void invcredentials(String username, String password) throws IOException, InterruptedException {
		Assert.assertTrue(lblLogin.isDisplayed());
		//cmnmethods.getPropFile();
		String emailID = prop.getProperty("uName");
		String  pwdtxt = prop.getProperty("invpwd");
		cmnmethods.inputData(inptEmail, emailID);
		cmnmethods.inputData(inptPassword, pwdtxt);
		cmnmethods.elmtClick(btnSubmit, 1000);
	}
	
	
	
}
	
