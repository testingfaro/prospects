Feature: Create Prospect

Background:
User login - Prospects Screen
Given user is on outplay login page in environment
|environment|stage|
When user log in with valid credentials
|username|uName|
|password|pWord|
Then Prospects is shown on the prospects page

Scenario Outline: PROSP_TC001_Create prospect
Given user is on Add Prospect modal window
When user fill all the details "<firstName>" "<lastName>" "<company>"
And create prospect "<firstName>" "<lastName>" "<company>"
Then prospect is created
Examples:
|firstName|lastName|company|
|Dirty|Martini|capgemini|

Scenario Outline: PROSP_TC001_Cancel prospect
Given user is on Add Prospect modal window
When user fill all the details "<firstName>" "<lastName>" "<company>" 
And cancel prospect "<firstName>" "<lastName>" "<company>"
Then prospect is not created
Examples:
|firstName|lastName|company|
|Great|Expectations|Mindtree|

Scenario: PROSP_TC001_Create prospect without mandatory fields
Given user is on Add Prospect modal window
When user create prospect without any values
Then error message is shown
"""
Provide Email or Phone or Linkedin
"""


Scenario Outline: PROSP_TC001_Create prospect with only email
Given user is on Add Prospect modal window
When user creates a prospect with only email "<firstName>" "<lastName>"
Then prospect is created
Examples:
|firstName|lastName|
|Leaves|Grass|



Scenario Outline: PROSP_TC001_Check prospect name when a prospect is created with Email but without Company & Account
Given user is on Add Prospect modal window 
When user creates a prospect with email & without company, account "<firstName>" "<lastName>"
And user creates a prospect with email, company but not account "<firstName>" "<lastName>" "<company>"
And user creates a prospect with email, company & account "<firstName>" "<lastName>" "<company>" "<account>"
Then prospect is created with prospect names accordingly
Examples:
|firstName|lastName|company|account|
|Mango|Fruiti|amazon|Existing Account|

Scenario Outline: PROSP_TC001_Create prospect with only phone number
Given user is on Add Prospect modal window
When user creates a prospect with only phone number "<firstName>" "<lastName>"
Then prospect is created
Examples:
|firstName|lastName|
|Lemon|Tea|

Scenario Outline: PROSP_TC001_Create prospect with only linkedin
Given user is on Add Prospect modal window
When user creates a prospect with only linkedin "<firstName>" "<lastName>"
Then prospect is created
Examples:
|firstName|lastName|
|Essential|Rumi|

Scenario Outline: PROSP_TC007_Create prospect with existing email id
Given user is on Add Prospect modal window
When user creates prospect with existing email id "<firstName>" "<lastName>"
Then error message is shown
"""
Prospect already exists
"""
Examples:
|firstName|lastName|
|Leaves|Grass|

Scenario Outline: PROSP_TC008_Create prospect with existing linkedin id
Given user is on Add Prospect modal window
When user creates prospect with existing linkedin id "<firstName>" "<lastName>"
Then error message is shown
"""
Prospect already exists
"""
Examples:
|firstName|lastName|
|Essential|Rumi|

Scenario: Select all prospects in a page
Given user is on prospects screen
When user select all prospects in a page
Then all prospects in the current page is selected

Scenario: Select all prospects in a page & clear selection
Given user is on prospects screen
When user select all prospects in a page
And clear the selection
Then all prospects in the current page is deselected

Scenario: Select all prospects in prospect list
Given user is on prospects screen
When user select all prospects in the prospect list
Then all prospects in the prospect list are selected

Scenario: Select all prospects in prospect list & clear selection
Given user is on prospects screen
When user select all prospects in the prospect list
And clear the selection
Then all prospects in the prospect list are deselected

Scenario Outline: Search Prospect using prospect name
Given user is on Add Prospect modal window
When user search prospect using prospect name "<firstName>" "<lastName>"
Then Prospect with relevant name is shown
Examples:
|firstName|lastName|
|Midnight|Garden|

Scenario Outline: Search Prospect using email
Given user is on prospects screen
When user search prospect using emailid "<firstName>" "<lastName>"
Then Prospect with relevant emailid is shown
Examples:
|firstName|lastName|
|Midnight|Garden|

Scenario: Search Prospect using any values other than prospect name & email
Given user is on prospects screen
When user search prospect using values other than prospect name & email
Then Prospect is not shown
@create
Scenario: Search Prospect field disappears when any prospect is selected
Given user is on prospects screen
When user selects any prospect
Then search prospect field is disappeared

