Feature: Login Functionality

Scenario Outline: Login outplay with incorrect credentials
Given user is on outplay login page in "<environment>" environment
When user enters invalid "<username>","<password>" and clicks login
Then Error message is shown
Examples:
|environment|username|password|
|stage|uName|invpwd|


Scenario Outline: Login outplay with correct credentials
Given user is on outplay login page in "<environment>" environment
When user enters valid "<username>","<password>" and clicks login
Then Dashboard is shown on the welcome page
Examples:
|environment|username|password|
|stage|uName|pWord|